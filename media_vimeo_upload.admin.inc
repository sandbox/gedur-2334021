<?php
/**
 * @file
 * Admin file.
 */

/**
 * Config form.
 */
function media_vimeo_upload_config_form($form, &$form_state) {
  $fields = media_vimeo_upload_config_form_field_types_options();
  if (!empty($fields)) {
    $form['media_vimeo_upload_fields'] = array(
      '#title'         => t('Which file fields do you want sync with Media Vimeo Upload'),
      '#type'          => 'checkboxes',
      '#options'       => $fields,
      '#default_value' => variable_get('media_vimeo_upload_fields', array()),
    );
  }
  else {
    $form['media_vimeo_upload_field_unexistent_copy'] = array(
      '#type'   => 'markup',
      '#markup' => t('No  fields available to sync.'),
    );
  }
  return system_settings_form($form);
}

/**
 * Obtains field types allowed for media vimeo upload.
 *
 * Extracts only fields of type  instances in a node, and provides drupal alter to
 * specific cases.
 *
 * @return array
 *   List of field of type .
 */
function media_vimeo_upload_config_form_field_types_options() {
  $fields = field_info_fields();
  $options = array();
  foreach ($fields as $field_name => $spec) {
    if ($spec['type'] == 'file' && isset($spec['bundles']['node'])) {
      $options[] = $field_name;
    }
  }
  drupal_alter('media_vimeo_upload_fields', $options);
  return drupal_map_assoc($options);
}
